package com.hellokoding.springboot;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.hellokoding.springboot.viewmodels.Student;

@Controller
public class HelloController {
    @RequestMapping("/hello")
    public String hello(Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) {
        model.addAttribute("name", name);
        System.out.println("hello");
        //model.add("student")
        return "hello";
    }
    
    
    
    @RequestMapping(value="/student/add", method = RequestMethod.POST)
    public String saveStudent(Student student,Model model ) {
       System.out.println(student);
       model.addAttribute("name", student);
        return "success";
    }
    
    
}
